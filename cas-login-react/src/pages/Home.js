import { useContext, useEffect } from "react";
import { casUserContext as casContext } from "../App";
import Clock from "../components/Clock";
import { useSearchParams } from "react-router-dom";
import useCas from "../hooks/useCas";

function Home() {
  const casUserContext = useContext(casContext);
  const { login, logout } = useCas();
  const { user } = casUserContext;

  const [searchParams] = useSearchParams();

  useEffect(() => {
    const status = searchParams.get("status");
    if (status === "in_process") {
      login();
    }
    // eslint-disable-next-line
  }, []);

  return (
    <div className="container">
      <Clock />
      <h1>
        Hello{" "}
        {user ? `${user}, you are login success` : "anonymous, please login!"}
      </h1>
      {user ? null : (
        <button className="button" onClick={() => login()}>
          Login with cas
        </button>
      )}
      {user ? (
        <button className="button" onClick={() => logout()}>
          Logout
        </button>
      ) : null}
    </div>
  );
}

export default Home;
