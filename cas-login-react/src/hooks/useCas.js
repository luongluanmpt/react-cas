import { useContext, useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import CasClient, { constant } from "react-cas-client";
import { casUserContext } from "../App";

const casEndpoint = "localhost:8080";
const redirectPath = "/";
const casOptions = { version: constant.CAS_VERSION_3_0, protocol: "http" };
const casClient = new CasClient(casEndpoint, casOptions);
const gateway = false;

function useCas() {
  const [isLogin, setIsLogin] = useState(false);
  const casContext = useContext(casUserContext);
  const navigate = useNavigate();

  const { user, setUser } = casContext;

  useEffect(() => {
    handleLogin();
    // eslint-disable-next-line
  }, [isLogin]);

  const login = () => setIsLogin(true);
  const logout = () => {
    setIsLogin(false);
    handleLogout();
  };

  const handleLogin = async () => {
    try {
      if (!user && isLogin) {
        const casReponse = await casClient.auth(gateway);
        setUser(casReponse.user);
        navigate("/");
      }
    } catch (error) {
      console.log(error);
    }
  };

  const handleLogout = () => {
    setUser(null);
    casClient.logout(redirectPath);
  };

  return { login, logout };
}

export default useCas;
