import { createContext, useState } from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import "./App.css";

import Home from "./pages/Home";
import Login from "./hooks/useCas";

export const casUserContext = createContext();

function App() {
  const [user, setUser] = useState(null);
  return (
    <casUserContext.Provider value={{ user, setUser }}>
      <BrowserRouter>
        <Routes>
          <Route path="/" Component={Home} />
          <Route path="/login" Component={Login} />
        </Routes>
      </BrowserRouter>
    </casUserContext.Provider>
  );
}

export default App;
